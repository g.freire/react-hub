import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// objeto sendo injetado no componente principal
let contacts = {
    name: 'gustavo',
    age: 28.25,
    adress: 'niteroi',
    state: 'rj',
    programmingLanguages: ['SQL', 'Python', 'Golang', 'C#', 'JS'],
    yearsProgramming: [2, 3, 1, 2, 4],
    hasDarkHair: true,
    hasRedHair: 0,
    measurement: { weight: 73.4, height: 1.70 },
    nationality: 'br',
    screenName: 'g-freire',
    businessPlan: 'enterprise'
}



ReactDOM.render(<App contacts={contacts} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
