import React, { Component } from 'react';

// inline style
const pStyle = {
    fontSize: '15px',
    textAlign: 'center',
    color: 'blue'
};

class Contracts extends Component {

    render() {
        return (
            <div>
                <h1 style={pStyle}>Contracts Component header</h1>
            </div>
        )

    }
}

export default Contracts;
