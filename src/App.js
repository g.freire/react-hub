import React, { Component } from 'react';
import './App.css';
import Contacts from './Contacts.js';
import Contracts from './Contracts.js';
import StateOnChange from './StateOnChange.js';
// app recebe os dados hardcodeds de index.js
class App extends Component {

  render() {
    // no retorno nao entra expressao com js
    console.log(this.props.contacts)
    // importa os componentes, html tags template
    return (
      <div>
        <h1>appComponent</h1>
        {/* nao da pra passar objeto inteiro, so props */}
        <div>{this.props.contacts.name}</div>
        <Contacts contacts={this.props.contacts}></Contacts>
        <Contracts></Contracts>
        <StateOnChange></StateOnChange>
      </div >
    )
  }
}

export default App;
  // export default only one (module)
  // can export other 