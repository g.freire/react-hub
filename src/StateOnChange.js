import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

class StateOnChange extends Component {

  state = {
    name: 'pepperoni'
  }
// bad
  changeName = (newName) => {
    this.setState({
      name: newName
    })
  }
// better
  changeNameFromInput = (event) => {
    this.setState({
      name: event.target.value
    })
  }

  render() {
    return (
      <div className="StateOnChange">
        <h1>teste mobile StateOnChange </h1>
        <button onClick={() => this.changeName('awesome-anon-function')}>Changed using ananonymous function =(</button>
        <br />
        <button onClick={this.changeName.bind(this, 'awesome-binded')}>Changed using bind object =)</button>
        <br /><br />
        <h5>onChange chama funcao que recebe event e set.State prop event target value</h5>

        <input type="text" onChange={this.changeNameFromInput} value={this.state.name} />

        <br /> <br />
        <div> <h3>{this.state.name}</h3></div>
      </div>
      // <Link to="/control_room"><button className="main_button">Control-Room</button></Link>

    );
  }
}

export default StateOnChange;
