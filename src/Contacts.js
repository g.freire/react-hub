import React, { Component } from 'react';
import './App.css';


// inline style
const pStyle = {
  fontSize: '15px',
  textAlign: 'center',
  color: 'blue'
};

class Contacts extends Component {


  render() {
    return (
      <div>
        <h1 style={pStyle}>contacts list component header</h1>
      </div>
    )

  }
}

// exportacao de modulo
export default Contacts;
